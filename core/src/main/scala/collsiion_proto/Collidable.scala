package collsiion_proto

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Rectangle

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

object Collidable {
    private var _nextId = 0
}

class Collidable(val rect: Rectangle) extends HasRect {
    val id = {
        val id = Collidable._nextId
        Collidable._nextId += 1
        id
    }
    private var _collidingWith = new ArrayBuffer[Collidable]()

    def collidingWith = _collidingWith.toList
    def colliding = _collidingWith.nonEmpty

    def update(): Unit = {
        rect.x = Random.nextInt(Gdx.graphics.getWidth)
        rect.y = Random.nextInt(Gdx.graphics.getHeight)

        //rect.x += 1
        //rect.y += 1
    }

    def collided(others: Seq[Collidable]): Unit = {
        _collidingWith.clear()
        _collidingWith ++= others
    }
}
