package collsiion_proto

import scala.collection.mutable.ArrayBuffer

class Naive extends CollisionEngine {
    private val _collidables = new ArrayBuffer[Collidable]()

    override def addCollidable(c: Collidable): Unit = {
        _collidables += c
    }

    override def removeCollidable(c: Collidable): Unit = {
        _collidables -= c
    }

    override def update(): Unit = {

    }

    override def query(c: Collidable): Seq[Collidable] = {
        val others = for {
            c2 <- _collidables
            if c != c2 && c.rect.overlaps(c2.rect)
        } yield c2
        others
    }
}
