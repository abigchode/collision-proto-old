package collsiion_proto

import java.util.{Comparator, Collections}
import com.badlogic.gdx.math.Rectangle

import scala.collection.mutable.ArrayBuffer

trait HasRect {
    def rect: Rectangle
}

class AxisBasedPartitioning2[T <: HasRect] {
    private val _rects = new java.util.ArrayList[T]()
    private val _rectComparator = new Comparator[T] {
        override def compare(r1: T, r2: T): Int = r1.rect.x.compareTo(r2.rect.x)
    }

    def addCollidable(r: T): Unit = {
        _rects.add(r)
    }

    def removeCollidable(r: T): Unit = {
        _rects.remove(r)
    }

    def update(): Unit = {
        Collections.sort(_rects, _rectComparator)

        /*var i = 0
        while(i < _rects.size()) {
            val c = _rects.get(i)
            var startIdx = i
            var startCollidable = _rects.get(startIdx)
            while(startIdx > 0 && startCollidable.rect.x + startCollidable.rect.width >= c.rect.x) {
                startIdx -= 1
                startCollidable = _rects.get(startIdx)
            }

            val collidedWith = new ArrayBuffer[Collidable]()
            var j = startIdx
            var break = false
            while(j < _rects.size() && !break) {
                val check = _rects.get(j)
                if(c.rect.x + c.rect.width < check.rect.x) {
                    break = true
                }
                else {
                    if(c != check && c.rect.overlaps(check.rect)) {
                        collidedWith += check
                    }
                }
                j += 1
            }
            c.collided(collidedWith)

            i += 1
        }*/
    }

    def query(r: Rectangle, filters: Seq[T => Boolean] = Nil): Seq[T] = {
        var startIdx = closestIndex(r)
        var startRect = _rects.get(startIdx)
        while(startIdx > 0 && startRect.rect.x + startRect.rect.width >= r.x) {
            startIdx -= 1
            startRect = _rects.get(startIdx)
        }

        val collidedWith = new ArrayBuffer[T]()
        var j = startIdx
        var break = false
        while(j < _rects.size() && !break) {
            val check = _rects.get(j)
            if(r.x + r.width < check.rect.x) {
                break = true
            }
            else {
                if(r.overlaps(check.rect) && filters.forall(_(check))) {
                    collidedWith += check
                }
            }
            j += 1
        }
        collidedWith
    }

    private def closestIndex(r: Rectangle): Int = {
        var splitSize = _rects.size() / 2
        var idx = splitSize

        while(splitSize > 1) {
            splitSize /= 2
            val current = _rects.get(idx)
            if(r.x < current.rect.x) {
                idx -= splitSize
            }
            else if(r.x > current.rect.x) {
                idx += splitSize
            }
            else {
                splitSize = 1
            }
        }

        idx
    }
}
