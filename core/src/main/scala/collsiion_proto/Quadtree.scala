package collsiion_proto

import com.badlogic.gdx.math.Rectangle

import scala.collection.mutable.ArrayBuffer

object Quadtree {
    val maxObjects = 10
    val minSize = 64
}

class Quadtree(parentOptionArg: Option[Quadtree], rectArg: Rectangle) extends CollisionEngine {
    private val _parentOption = parentOptionArg
    private val _rect = rectArg
    private val _collidables = new ArrayBuffer[Collidable]()
    private var _nodes: Option[Array[Quadtree]] = None

    def rect = _rect
    def nodes = _nodes

    override def addCollidable(c: Collidable): Unit = {
        _nodes match {
            case Some(nodes) =>
                val containingNodes = for {
                    node <- nodes
                    if node._rect.contains(c.rect)
                } yield node
                if(containingNodes.length == 1) {
                    containingNodes.head.addCollidable(c)
                }
                else {
                    _collidables += c
                }
            case None =>
                _collidables += c
        }

        if(_nodes.isEmpty && _collidables.length > Quadtree.maxObjects && _rect.width > Quadtree.minSize && _rect.height > Quadtree.minSize) {
            val halfWidth = _rect.width / 2
            val halfHeight = _rect.height / 2
            val nodes = Array(
                new Quadtree(Some(this), new Rectangle(_rect.x, _rect.y, halfWidth, halfHeight)),
                new Quadtree(Some(this), new Rectangle(_rect.x + halfWidth, _rect.y, halfWidth, halfHeight)),
                new Quadtree(Some(this), new Rectangle(_rect.x + halfWidth, _rect.y + halfHeight, halfWidth, halfHeight)),
                new Quadtree(Some(this), new Rectangle(_rect.x, _rect.y + halfHeight, halfWidth, halfHeight))
            )
            _nodes = Some(nodes)

            var i = 0
            while(i < _collidables.length) {
                val containingNodes = for {
                    node <- nodes
                    if node._rect.contains(_collidables(i).rect)
                } yield node
                if(containingNodes.length == 1) {
                    containingNodes.head.addCollidable(_collidables.remove(i))
                }
                else {
                    i += 1
                }
            }
        }
    }

    override def removeCollidable(c: Collidable): Unit = {

    }

    override def update(): Unit = {
        var i = 0
        while(i < _collidables.length) {
            val c = _collidables.remove(i)
            var top = this
            while(top._parentOption.nonEmpty && !top._rect.contains(c.rect)) {
                top = top._parentOption.get
            }
            top.addCollidable(c)
            i += 1
        }

        _nodes.foreach { nodes =>
            for(node <- nodes) {
                node.update()
            }

            val nodeUsage = for {
                node <- nodes
            } yield if(node.nodes.nonEmpty) Quadtree.maxObjects + 1 else node._collidables.length

            if(nodeUsage.sum + _collidables.length <= Quadtree.maxObjects) {
                for (node <- nodes) {
                    _collidables ++= node._collidables
                }
                _nodes = None
            }
        }
    }

    override def query(c: Collidable): Seq[Collidable] = {
        val subCollidables = for {
            nodes <- _nodes.iterator
            node <- nodes
            if node._rect.overlaps(c.rect)
        } yield node.query(c)

        subCollidables.flatten.toSeq ++ _collidables.filter(c2 => c != c2 && c2.rect.overlaps(c.rect))
    }
}
