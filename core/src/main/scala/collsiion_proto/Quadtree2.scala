package collsiion_proto

import com.badlogic.gdx.math.Rectangle

import collection.mutable
import scala.language.implicitConversions

class Node(val rect: Rectangle) {
    val collidables = mutable.Buffer[Collidable]()
    var children: Array[Node] = null

    def whichChild(collidable: Collidable): Int = {
        (if(collidable.rect.x > rect.x) 1 else 0) + (if(collidable.rect.y > rect.y) 2 else 0)
    }
    def makeChildren() {
        val halfWidth = rect.width / 2
        val halfHeight = rect.height / 2
        children = Array(
            new Node(new Rectangle(rect.x, rect.y, halfWidth, halfHeight)),
            new Node(new Rectangle(rect.x + halfWidth, rect.y, halfWidth, halfHeight)),
            new Node(new Rectangle(rect.x, rect.y + halfHeight, halfWidth, halfHeight)),
            new Node(new Rectangle(rect.x + halfWidth, rect.y + halfHeight, halfWidth, halfHeight))
        )
    }
}

// code largely taken from: http://www.cs.trinity.edu/~mlewis/CSCI1321-F11/Code/src/util/Quadtree.scala
class Quadtree2 (
    xmin: Float,
    ymin: Float,
    xmax: Float,
    ymax: Float,
    val maxCollidablesPerSquare: Int,
    val minWidth: Int,
    val minHeight: Int
) {
    val root = new Node(new Rectangle(xmin, ymin, xmax - xmin, ymax - ymin))

    def addCollidable(collidable: Collidable) {
        addRecur(collidable, root)
    }

    private def addRecur(collidable: Collidable, n: Node) {
        if(n.children == null) {
            if(n.collidables.length < maxCollidablesPerSquare || n.rect.width <= minWidth || n.rect.height <= minHeight) {
                n.collidables += collidable
            }
            else {
                n.makeChildren()
                for(o <- n.collidables) {
                    addRecur(o, n.children(n.whichChild(o)))
                }
                n.collidables.clear()
                addRecur(collidable, n.children(n.whichChild(collidable)))
            }
        }
        else {
            addRecur(collidable, n.children(n.whichChild(collidable)))
        }
    }

    def query(collidable: Collidable): mutable.Buffer[Collidable] = {
        val ret = mutable.Buffer[Collidable]()
        searchRecur(collidable, root, ret)
        ret
    }

    private def searchRecur(collidable: Collidable, n: Node, ret: mutable.Buffer[Collidable]) {
        if(n.children == null) {
            ret ++= n.collidables.filter(_.rect.overlaps(collidable.rect))
        } else {
            for {
                child <- n.children
                if child.rect.contains(collidable.rect)
            } {
                searchRecur(collidable, child, ret)
            }
        }
    }

    def clear() {
        root.collidables.clear()
        root.children = null
    }
}