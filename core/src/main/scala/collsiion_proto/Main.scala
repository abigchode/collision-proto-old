package collsiion_proto

import com.badlogic.gdx.Game

object Main extends Game {
    private var _game: CollisionProto = null

    override def create(): Unit = {
        _game = new CollisionProto()
        setScreen(_game)
    }
}
