package collsiion_proto

trait CollisionEngine {
    def addCollidable(c: Collidable)
    def removeCollidable(c: Collidable)
    def update()
    def query(c: Collidable): Seq[Collidable]
}
