package collsiion_proto

import java.util.{Collections, Comparator}

import scala.collection.JavaConverters._

class AxisBasedPartitioning extends CollisionEngine {
    private val _collidables = new java.util.ArrayList[Collidable]()
    private var _scalaCollidables = _collidables.asScala

    def addCollidable(c: Collidable): Unit = {
        _collidables.add(c)
    }

    def removeCollidable(c: Collidable): Unit = {
        _collidables.remove(c)
    }

    def update(): Unit = {
        Collections.sort(_collidables, new Comparator[Collidable] {
            override def compare(c1: Collidable, c2: Collidable): Int = c1.rect.x.compareTo(c2.rect.x)
        })

        _scalaCollidables = _collidables.asScala
    }

    def query(c: Collidable) = {
        val leftDropped = _scalaCollidables.dropWhile(c2 => c2.rect.x + c2.rect.width < c.rect.x)
        val xOverlap = leftDropped.takeWhile(c2 => c2.rect.x < c.rect.x + c.rect.width)
        val collidedWith = xOverlap.filter(c2 => c != c2 && c.rect.overlaps(c2.rect))
        collidedWith
    }
}
