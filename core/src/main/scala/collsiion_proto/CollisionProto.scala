package collsiion_proto

import com.badlogic.gdx.Input.Buttons
import com.badlogic.gdx.graphics.g2d.{SpriteBatch, BitmapFont}
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.{InputProcessor, Gdx, Screen}
import com.badlogic.gdx.graphics.{FPSLogger, GL20, OrthographicCamera, Color}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class CollisionProto extends Screen {
    private val _fpsLogger = new FPSLogger()
    private val _gos = new ArrayBuffer[Collidable]()
    private var _collisionEngine =
        //new Naive()
        //new AxisBasedPartitioning()
        //new Quadtree(None, new Rectangle(0, 0, Gdx.graphics.getWidth, Gdx.graphics.getHeight))
        //new Quadtree2(0, 0, Gdx.graphics.getWidth, Gdx.graphics.getHeight, 10, 64, 64)
        new AxisBasedPartitioning2[Collidable]()
    private val _camera = new OrthographicCamera()
    private val _shapeRenderer = new ShapeRenderer()
    private val _spriteBatch = new SpriteBatch()
    private val _font = {
        val assetsDir = Gdx.files.internal("target")
            .list.filter(_.name.startsWith("scala")).head
            .child("classes")
        new BitmapFont(assetsDir.child("fonts").child("default.fnt"))
    }
    private var _frame = 0

    {
        for(i <- 0 until 4000) {
            addCollidable(Random.nextInt(Gdx.graphics.getWidth), Random.nextInt(Gdx.graphics.getHeight))
        }
        /*for {
            i <- 0 until 60
            j <- 0 until 33
        } {
            addCollidable(i * 32, j * 32)
        }*/
        Gdx.input.setInputProcessor(new InputProcessor {
            override def keyTyped(character: Char): Boolean = true
            override def mouseMoved(screenX: Int, screenY: Int): Boolean = true
            override def keyDown(keycode: Int): Boolean = true
            override def touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = {
                button match {
                    case Buttons.LEFT =>
                        _gos.foreach(_.update())
                    case Buttons.RIGHT =>
                        var failed = false
                        for {
                            go1 <- _gos
                            go2 <- _gos
                            if go1 != go2
                        } {
                            if(go1.rect.overlaps(go2.rect)) {
                                if(!go1.collidingWith.contains(go2) || !go2.collidingWith.contains(go1)) {
                                    println("failing to detect collision")
                                    failed = true
                                }
                            }
                            else {
                                if(go1.collidingWith.contains(go2) || go2.collidingWith.contains(go1)) {
                                    println("detecting collsion when none present")
                                    failed = true
                                }
                            }
                        }
                        if(!failed) {
                            println("no errors detected")
                        }
                }
                true
            }
            override def keyUp(keycode: Int): Boolean = true
            override def scrolled(amount: Int): Boolean = true
            override def touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = true
            override def touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean = true
        })
    }

    private def addCollidable(x: Int, y: Int): Unit = {
        val collidable = new Collidable(new Rectangle(x, y, 32, 32))
        _gos += collidable
        _collisionEngine.addCollidable(collidable)
    }

    override def render(delta: Float): Unit = {
        update()
        draw()
        _fpsLogger.log()
    }

    private def update(): Unit = {
        //if(_frame % 300 == 0) {
            _gos.foreach(_.update())
        //}
        _collisionEngine.update()
        //_collisionEngine = new Quadtree(None, new Rectangle(0, 0, Gdx.graphics.getWidth, Gdx.graphics.getHeight))
        //_gos.foreach(go => _collisionEngine.addCollidable(go))
        _gos.foreach { c =>
            //val testRect = new Rectangle(c.rect.x - 32, c.rect.y - 32, 96, 96)
            val testRect = new Rectangle(c.rect)
            val others = _collisionEngine.query(testRect, Seq(c2 => c2 != c))
            c.collided(others)
        }
        _frame += 1
    }

    private def draw(): Unit = {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        _shapeRenderer.setProjectionMatrix(_camera.combined)
        _spriteBatch.setProjectionMatrix(_camera.combined)



        _shapeRenderer.begin(ShapeType.Line)
        _gos.foreach { go =>
            if(go.colliding) {
                _shapeRenderer.setColor(Color.RED)
            }
            else {
                _shapeRenderer.setColor(Color.GREEN)
            }
            _shapeRenderer.rect(go.rect.x, go.rect.y, go.rect.width, go.rect.height)
        }
        _shapeRenderer.end()



        _spriteBatch.begin()
        _gos.foreach { go =>
            _font.draw(_spriteBatch, go.id.toString, go.rect.x, go.rect.y + go.rect.height)
        }
        _spriteBatch.end()



        _shapeRenderer.begin(ShapeType.Line)
        _shapeRenderer.setColor(Color.WHITE)
        _collisionEngine match {
            case qt: Quadtree =>
                def drawQuad(qt: Quadtree): Unit = {
                    _shapeRenderer.rect(qt.rect.x, qt.rect.y, qt.rect.width, qt.rect.height)
                    for {
                        nodes <- qt.nodes
                        node <- nodes
                    } drawQuad(node)
                }
                drawQuad(qt)
            case qt: Quadtree2 =>
                def drawNode(n: Node): Unit = {
                    if(n.children != null) {
                        for(child <- n.children) drawNode(child)
                    }
                    _shapeRenderer.rect(n.rect.x, n.rect.y, n.rect.width, n.rect.height)
                }
                drawNode(qt.root)
            case _ =>
        }
        _shapeRenderer.end()
    }

    override def resize(width: Int, height: Int): Unit = {
        _camera.setToOrtho(false, width, height)
    }

    override def hide(): Unit = {}
    override def dispose(): Unit = {}
    override def pause(): Unit = {}
    override def show(): Unit = {}
    override def resume(): Unit = {}
}
