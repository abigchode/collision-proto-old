package collision_proto

import collsiion_proto.Main
import com.badlogic.gdx.backends.lwjgl._

object DesktopMain extends App {
    val cfg = new LwjglApplicationConfiguration
    cfg.title = "collision-proto"
    cfg.height = 1080
    cfg.width = 1920
    cfg.forceExit = false
    new LwjglApplication(Main, cfg)
}
